import json
import os
import pickle

import pandas as pd

# "run_locally": "az ml model deploy -n app-mark-local -m model_mark:3 --ic ./mlops/inference_config_v2.json --dc ./mlops/deployment_config_local.json --overwrite -v"
# "get logs": "az ml service get-logs --verbose --workspace-name Deliverable-AML --name app-mark"
# "local debug": "https://docs.microsoft.com/en-us/azure/machine-learning/how-to-troubleshoot-deployment?tabs=azcli#dockerlog"
# https://docs.microsoft.com/en-us/azure/machine-learning/how-to-troubleshoot-deployment?tabs=azcli#debug-locally


def init():
    """Init when service is starting"""
    global model
    with open(
        os.path.join(os.getenv("AZUREML_MODEL_DIR", "."), "model/model.pkl"), "rb"
    ) as model_file:
        model = pickle.load(model_file)
    print("This is init")


def run(data):
    """Performed with each request"""
    print(f"+++++++++++ data: {data}")
    input_data = json.loads(data)
    print(f"+++++++++++ received data: {input_data}")
    prediction = model.predict(pd.DataFrame(input_data))
    print(f"+++++++++++ prediction: {prediction}")
    return f"Prediction: {prediction}"
