import json


def init():
    """Load model"""
    print("This is init")


def run(data):
    """Run on each request"""
    test = json.loads(data)
    print(f"received data {test}")
    return f"test is {test}"
