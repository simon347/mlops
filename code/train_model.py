# %%
import os

import mlflow
from azureml.core import Workspace, authentication
from dotenv import load_dotenv
from sklearn import datasets, metrics
from sklearn.model_selection import train_test_split
from xgboost.sklearn import XGBClassifier

load_dotenv()

# %%
# User authentication
interactive_auth = authentication.InteractiveLoginAuthentication(tenant_id=os.environ["TENANT_ID"])

# %%
# Get workspace environment
ws = Workspace.get(
    name=os.environ["WORKSPACE_NAME"],
    auth=interactive_auth,
    subscription_id=os.environ["SUBSCRIPTION_ID"],
    resource_group=os.environ["RESOURCE_GROUP"],
)

# Setup mlflow tracking to Azure Machine Learning workspace
mlflow.set_tracking_uri(ws.get_mlflow_tracking_uri())

# Set experiment name to user specific name to start logging user specific training runs
mlflow.set_experiment(os.environ["EXPERIMENT_NAME"])

# %%
# Load data
digits = datasets.load_digits(as_frame=True)
X, y = digits.data, digits.target
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2, random_state=42)

# %%
# Start training run logged to mlflow
with mlflow.start_run():
    # set xgboost params
    params = {
        "max_depth": 100,  # the maximum depth of each tree
        "eta": 0.0001,  # the training step for each iteration
        "objective": "multi:softprob",  # error evaluation for multiclass training
        "num_class": 10,  # the number of classes that exist in this dataset
        "eval_metric": "mlogloss",
    }
    mlflow.log_params(params)

    # training and testing
    model = XGBClassifier(**params, use_label_encoder=False)
    model.fit(X_train, y_train)
    y_pred = model.predict(X_test)

    # log results to mlflow
    metrics_results = {
        "precision": metrics.precision_score(y_test, y_pred, average="macro"),
        "accuracy": metrics.accuracy_score(y_test, y_pred),
        "f1": metrics.f1_score(y_test, y_pred, average="macro"),
    }
    mlflow.log_metrics(metrics_results)

    # Set ready_for_reg to true when model tuning is finished
    ready_for_reg = False
    mlflow.set_tag("ready_for_reg", ready_for_reg)

    # Save the model to the outputs directory for capture
    mlflow.sklearn.log_model(
        sk_model=model,
        artifact_path="model",
        registered_model_name=os.environ["MODEL_NAME"] if ready_for_reg else None,
        extra_pip_requirements="extra_deploy_requirements.txt",
    )

# %%
