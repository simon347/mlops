# %%
import os

from azureml.core import Environment, Workspace, authentication
from azureml.core.model import InferenceConfig, Model
from azureml.core.webservice import AciWebservice, LocalWebservice
from dotenv import load_dotenv

load_dotenv()

# User authentication
interactive_auth = authentication.InteractiveLoginAuthentication(tenant_id=os.environ["TENANT_ID"])

# Get workspace environment
ws = Workspace.get(
    name=os.environ["WORKSPACE_NAME"],
    auth=interactive_auth,
    subscription_id=os.environ["SUBSCRIPTION_ID"],
    resource_group=os.environ["RESOURCE_GROUP"],
)

source_directory = "."
model = Model(ws, os.environ["MODEL_NAME"])
model.download(target_dir=source_directory, exist_ok=True)

# NOT WORKING - Default environment: https://docs.microsoft.com/en-us/azure/machine-learning/how-to-use-environments#implicitly-use-the-default-environment
# env = Environment(name="model_environment")

# WORKING - Use conda dependencies: https://docs.microsoft.com/en-us/azure/machine-learning/how-to-use-environments#use-conda-dependencies-or-pip-requirements-files
env = Environment.from_conda_specification(
    name="model_environment", file_path=f"{source_directory}/model/conda.yaml"
)
env.docker.base_image = "mcr.microsoft.com/azureml/openmpi3.1.2-cuda10.1-cudnn7-ubuntu18.04"
# env.docker.base_image = "mcr.microsoft.com/azureml/sklearn-0.24.1-ubuntu18.04-py37-cpu-inference:20220411.v1"

# Use docker base image and conda dependencies
# env = Environment.from_docker_image(name="model_environment",image="mcr.microsoft.com/azureml/sklearn-0.24.1-ubuntu18.04-py37-cpu-inference:20220411.v1", conda_specification=f"{source_directory}/model/conda.yaml")
# env.inferencing_stack_version = "latest"

# NOT WORKING - Curated environment: https://docs.microsoft.com/en-us/azure/machine-learning/how-to-use-environments#use-a-curated-environment
# env = Environment.get(workspace=ws,name="AzureML-sklearn-0.24.1-ubuntu18.04-py37-cpu-inference",name="AzureML-xgboost-0.9-ubuntu18.04-py37-cpu-inference",version=1)
# env.inferencing_stack_version = "latest"

# entry_script = "deployment/score_v1.py"
entry_script = "deployment/score_v2.py"
inference_config = InferenceConfig(
    environment=env,
    source_directory=source_directory,
    entry_script=entry_script,
)

deployment_config_local = LocalWebservice.deploy_configuration(port=6789)
deployment_config_aci = AciWebservice.deploy_configuration(cpu_cores=0.5, memory_gb=1)

# %% Deployment
deploy_aci = False
service = Model.deploy(
    workspace=ws,
    name=os.environ["SERVICE_NAME"],
    models=[model],
    inference_config=inference_config,
    deployment_config=deployment_config_aci if deploy_aci else deployment_config_local,
    overwrite=True,
)
service.wait_for_deployment(show_output=True)

print(service.get_logs())
print(f"Scoring URI is : {service.scoring_uri}")

# %%
