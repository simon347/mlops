# %%
# send raw HTTP request to test the web service.
import json

import requests
from sklearn import datasets
from sklearn.model_selection import train_test_split

# Local debugging/validation after deploy_model_local.py
scoring_uri = "http://0.0.0.0:6789/score"

# Local debugging/validation after https://docs.microsoft.com/en-us/azure/machine-learning/how-to-troubleshoot-deployment?tabs=azcli#azure-machine-learning-inference-http-server
# scoring_uri = "http://127.0.0.1:5001/score"

# ACI debugging/validation
# scoring_uri = "http://cdfa8efd-971a-4aaa-b3fc-e18e4899fca3.westeurope.azurecontainer.io/score"

# %%
# score_v1.py
# input_data = '{"data": "test"}'

# score_v2.py
digits = datasets.load_digits(as_frame=True)
X, y = digits.data, digits.target
_, X_test, _, y_test = train_test_split(X, y, test_size=0.2, random_state=42)
input_data = X_test.head().to_dict(orient="split")["data"]

# %%
headers = {"Content-Type": "application/json"}
resp = requests.post(scoring_uri, json.dumps(input_data), headers=headers)

print("POST to url", scoring_uri)
print("Labels y_test:", y_test.head().values)
print("Prediction endpoint:", resp.text)
# %%
