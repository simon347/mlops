import joblib
from sklearn import datasets, metrics
from sklearn.model_selection import train_test_split
from xgboost.sklearn import XGBClassifier

digits = datasets.load_digits(as_frame=True)
X, y = digits.data, digits.target
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2, random_state=42)

# set xgboost params
params = {
    "max_depth": 2,  # the maximum depth of each tree
    "eta": 0.00001,  # the training step for each iteration
    "objective": "multi:softprob",  # error evaluation for multiclass training
    "num_class": 10,  # the number of classes that exist in this dataset
    "eval_metric": "mlogloss",
}

# training and testing
model = XGBClassifier(**params, use_label_encoder=False)
model.fit(X_train, y_train)
y_pred = model.predict(X_test)

# check results on test set
print(metrics.classification_report(y_test, y_pred, target_names=digits.target_names.astype(str)))

# save the model for later
_ = joblib.dump(model, "model.joblib")
