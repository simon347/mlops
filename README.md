## Contents

- [Contents](#contents)
- [Setting up the environment](#setting-up-the-environment)
- [VSCode](#vscode)
- [Ensuring consistent code quality checks](#ensuring-consistent-code-quality-checks)
  - [Configuration files](#configuration-files)
  - [Linting and formatting](#linting-and-formatting)
    - [flake8](#flake8)
    - [black](#black)
    - [isort](#isort)
  - [pre-commit](#pre-commit)

## Setting up the environment
To create and activate a venv with all the correct packages, run in the current directory:
```
conda env create -f environment.yml --force
conda activate mlops
```
or without conda
```
python -m venv .venv
source .venv/bin/activate
pip install -r requirements.txt
```
When you create a new .py or .ipynb file, make sure that this Python venv is selected as Python interpreter (ctrl+shift+p, Python: Select Interpreter).
## VSCode
In this example we are using VSCode for our development. To run Python code we use "Interactive window".
Some VSCode settings are in [.vscode/settings.json](.vscode/settings.json) to ensure that development works the same for every developer. You can edit your own user settings using ctrl+shift+p by entering "Open settings" or "Open settings (JSON)" the first one is good for seeing which settings there are, the second one is good for seeing what you have set differently from the default setting and for copying settings as text. Here are some settings which are recommended to put in your JSON settings:
```
"csv-preview.separator": "[,;\t]",
"editor.bracketPairColorization.enabled": true,
"files.insertFinalNewline": true,
"git.autofetch": true,
"git.enableSmartCommit": true,
"jupyter.sendSelectionToInteractiveWindow": true,
"python.analysis.autoImportCompletions": true,
"python.analysis.completeFunctionParens": true,
```

If you add this to your Keyboard Shortcuts (JSON), pressing ctrl+enter in a regular Python file (without code cells) will run the complete file in the interactive window:
```
 {
  "key": "ctrl+enter",
  "command": "jupyter.runFileInteractive",
  "when": "editorTextFocus && isWorkspaceTrusted && !jupyter.hascodecells && !editorHasSelection && !jupyter.havenativecells && !notebookEditorFocused"
 }
```


## Ensuring consistent code quality checks
### Configuration files
Please have a look at the following files that contain configuration for our dev env:
- [.vscode/settings.json](.vscode/settings.json) contains settings that will automatically be used when you open this folder in [VSCode](#vscode)
- [.vscode/extensions.json](.vscode/extensions.json) contains a list of VSCode extensions that are recommended when you open this folder in [VSCode](#vscode)
- [.gitignore](.gitignore) contains patterns that are automatically excluded from being tracked by git (e.g. files that are automatically generated and large data files which are not efficient to store in git)
- [.pre-commit-config.yaml](.pre-commit-config.yaml) contains the settings for [pre-commit](#pre-commit)
- [pyproject.toml](pyproject.toml) contains the settings for [formatting](#linting-and-formatting) tools `black` and `isort`
- [setup.cfg](setup.cfg) contains the settings for the [linting](#linting-and-formatting) tool `flake8`
- [environment.yml](environment.yml) contains the list of packages conda should put in our environment

### Linting and formatting

#### flake8
The package `flake8` is used for linting, it will inform you if your code has potential bugs or clarity/readability issues. It helps you write code that you will still be able to understand in 2 years and that your coworkers will be able to understand now. Writing good code is more work than writing bad code, don't blame flake8 if it is bugging you a lot. If it gives warnings that you think are useless, add a comment telling flake8 to ignore the problem for once, or add them to the exceptions in [setup.cfg](setup.cfg).

#### black
`black` goes a little further than just telling you about unclear code, it actually formats it for you according to the Python coding style guideline (PEP8). The first time your code jumps around when you save it will be a bit confusing, but when you get used to it there are many advantages:
- Everyone's code has the same deterministic style, nobody can change/argue about it anymore
  - This makes the code more readable to others since everybody gets used to this style
  - This stops everyone from e.g. writing a line of code that is 200 characters long that nobody can read anymore in the future
- Every change in git is a change in code, not in formatting of the code
- You can make a bit of a mess of your code, black will clean it up when you save the file

#### isort
Similar to `black`, `isort` formats your imports for you. The agreement between Python developers is that imports from built-in Python packages should go first, then external (third-party) packages like pandas, and lastly imports from your own code. If this is applied everywhere it is immediately clear what kind of functionality you are using. With `isort` you don't need to check where the import should be, just import and let `isort` fix it for you.

### pre-commit
Pre-commit is a tool that can run quality checks (like `flake8`,`black`,`isort`) on the files that are part of a git commit. It is installed by poetry in the venv we are using and has to be enabled by running in this folder:
```
pre-commit install
```
If pre-commit makes any changes to your files it will give a warning when you try to commit, the changes made by pre-commit will be visible as new "Changed files" in VSCode.
